import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PobieranieComponent } from './pobieranie/pobieranie.component';

const routes: Routes = [
  {
    path: '',
    component: PobieranieComponent,
    data: { title: 'Pobieranie Get' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
