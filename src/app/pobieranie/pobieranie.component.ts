import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-pobieranie',
  templateUrl: './pobieranie.component.html',
  styleUrls: ['./pobieranie.component.css']
})
export class PobieranieComponent implements OnInit {

  elements:any = []; // typ any - jest to typ, który odpowiada dowolnemy typowi danych
                     // (więc nie musimy określić dokładnego typu obiekt)

  constructor(
    public rest:RestService // inicjalizacja obiektu klasy RestService
  ) { }

  ngOnInit() {
    // Tutaj wpisujemy wszystko co chcemy, aby wykonało się na początku ładowania komponentu
    this.getElements();
  }


  getElements() {
    this.elements = []; // Czyścimy listę dla pewności
    this.rest.getElements() // Odwołujemy się to metody z obiektu klasy RestService
      .subscribe( // Subscribe - służy do oczekiwania na odpowiedź,
                  // a następnie pozwala przekierować obsłużyć tą odpowiedź
                  // Wszystkie odpowiedzi do serwera są wykonywane asynchronicznie
                  // dlatego zwykłe przypisane this.elements = this.rest.getElements()
                  // nie zadziała, bo zwróci nam handler to wiszącego zapytania
        (data: any) => { // Ta struktura pojawia się też w Javie - jest to wyrażenie lambda
                        // czyli to co zostało zwrócone w zapytaniu przypisujemy do data, ustalamy typ na {} JSONA
                        // A następnie dla tej danej wykonujemy jakąś funkcję
      console.log(data); // W tym przypadku wypisanie danych
      this.elements = data.items; // przypisanie danych - z całego JSONa odwołuję się do tabeli items
    });
  }
}
